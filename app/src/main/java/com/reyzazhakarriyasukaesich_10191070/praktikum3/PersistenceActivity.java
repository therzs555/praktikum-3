package com.reyzazhakarriyasukaesich_10191070.praktikum3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.prefs.Preferences;


public class PersistenceActivity extends AppCompatActivity {

    private TextView Hasil;
    private EditText Masukan;
    private Button Eksekusi;

    private SharedPreferences preferences;

    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_persistence);
        Masukan = findViewById(R.id.input);
        Hasil = findViewById(R.id.output);
        Eksekusi = findViewById(R.id.save);

        preferences = getSharedPreferences("Belajar_SharedPreferences",
                Context.MODE_PRIVATE);

        Eksekusi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData();
                Toast.makeText(getApplicationContext(),"Data Tersimpan",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getData(){
        String getKonten = Masukan.getText().toString();
        editor = preferences.edit();
        editor.putString("data_saya", getKonten);
        editor.apply();

        Hasil.setText("Output Data : "+preferences.getString("data_saya", null));
    }
}