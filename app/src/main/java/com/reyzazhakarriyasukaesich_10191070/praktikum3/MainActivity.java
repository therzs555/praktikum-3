package com.reyzazhakarriyasukaesich_10191070.praktikum3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button interfaceButton, persistenceButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        interfaceButton = findViewById(R.id.interface_btn);
        interfaceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, InterfaceActivity.class);
                startActivity(intent);
            }
        });

        persistenceButton = findViewById(R.id.persistence_btn);
        persistenceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, PersistenceActivity.class);
                startActivity(intent);
            }
        });
    }
}
